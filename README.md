# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
| Gladson de Andrade Santos | @Gladsonandrade |
| Gustavo palaoro | @Palaoro |

# Documentação

A documentação do projeto pode ser acessada pelo link:

 https://gladsonandrade.gitlab.io/ie21cp20192

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)