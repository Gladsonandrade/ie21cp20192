# Documentação do Bebedouro Automático 
 
 
## Introdução 
 
 
 Neste projeto vamos desenvolver um sistema de acionamento para mini Bomba de Água que será controlada através de um sistema de aproximação por sensor Ultrassônico, para quando detectar um copo ou uma xícara acione a Bomba d'água e enche com o recipiente com água,suco ou refrigerante

## Objetivo
O obejtivo desse projeto é automatizar um bebedouro, para tornar facil a ultilização do mesmo.

## Resultados

### Video de demonstração 

{{< youtube "4BS9kAjHipw" >}}





### Esquema Elétrico

![](foto1.jpg)


## Materiais
* Arduino UNO + Cabo USB AB;
* Módulo Ultrassônico HC-SR04;
* Fonte de Alimentação 12V 2A;
* Módulo Relé 1 Canal;
* Bomba D Água;


## Desafios encontrados

Um dos desafios encontrado foi na busca da bomba D Água, pois foi difícil encotrar na região.
Outro desafio foi o codigo, pois tivemos que calcular a distância certa para que a bomba ative sem que houvesse derramaneto do liquido fora do copo. 


### Projeto Idealizado pelos acadêmicos

![](foto2.jpeg)


**Gladson de Andrade Santos**

**Gustavo Palaoro**



